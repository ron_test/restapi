import json


def show_students():
    """
    function that shows all students
    :return: list of all students information
    """
    pass


def show_student(id: int):
    """
    function that shows student info
    id: student id
    :return: student information
    """
    pass


def create_student(body: json):
    """
    function that creates new student
    body: student info
    :return: success or not
    """
    pass


def change_student_info(student_info: json):
    """
    function that shows student info
    student_info: student info with changed data
    :return: success or not
    """
    pass


def delete_student(id: int):
    """
    function that delete student info
    id: student id
    :return: success or not
    """
    pass
